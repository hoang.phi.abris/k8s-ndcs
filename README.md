# Kubernetes solution for ndcs

## What is Kubernetes 

Kubernetes is a container orchestration which provides:


- **Service discovery and load balancing** Kubernetes can expose a container using the **DNS name** or using **their own IP address**. If traffic to a container is high, Kubernetes is able to load balance and distribute the network traffic so that the deployment is stable.Storage orchestration Kubernetes allows you to automatically mount a storage system of your choice, such as local storages, public cloud providers, and more.

- **Automated rollouts and rollbacks** You can describe the desired state for your deployed containers using Kubernetes, and it can change the actual state to the desired state at a controlled rate. For example, you can automate Kubernetes to create new containers for your deployment, remove existing containers and adopt all their resources to the new container.

- **Automatic bin packing** You provide Kubernetes with a cluster of nodes that it can use to run containerized tasks. You tell Kubernetes how much CPU and memory (RAM) each container needs. Kubernetes can fit containers onto your nodes to make the best use of your resources.

- **Self-healing** Kubernetes restarts containers that fail, replaces containers, kills containers that don't respond to your user-defined health check, and doesn't advertise them to clients until they are ready to serve.

- **Secret and configuration management** Kubernetes lets you store and manage sensitive information, such as passwords, OAuth tokens, and SSH keys. You can deploy and update secrets and application configuration without rebuilding your container images, and without exposing secrets in your stack configuration.

## What Kubernetes is not

Kubernetes is not a traditional, all-inclusive PaaS (Platform as a Service) system. 

Since Kubernetes operates at the container level rather than at the hardware level, it provides some generally applicable features common to PaaS offerings, such as deployment, scaling, load balancing, and lets users integrate their logging, monitoring, and alerting solutions. 

However, Kubernetes is **not monolithic**, and these default solutions are optional and pluggable. Kubernetes provides the **building blocks for building developer platforms**, but preserves user choice and flexibility **where it is important**.

## Components

![Components](images/components.PNG)

### Pod

Pods are the smallest deployable units of computing that you can create and manage in Kubernetes.

### Service

In Kubernetes, a Service is an abstraction which defines a logical set of Pods and a policy by which to access them (sometimes this pattern is called a micro-service). 

### Ingress

You can use Ingress to expose your Service. Ingress is not a Service type, but it acts as the entry point for your cluster.

## Helm

Kubernetes package management. Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.

## Helm charts overview

Helm charts are used to deploy an application, or one component of a larger application. 

## Helm commands

- Step 1: Enable kubernetes on docker and download helm from [here](helm.sh)
- Step 2: Install helm ngnix chart
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx-controller ingress-nginx/ingress-nginx
```
- Step 3: Install following charts
```
helm install ndcs-client-tafc ndcs-client-tafc
helm install ndcsclienttafjdb ndcsclienttafjdb
helm install ndcs-client-tafj ndcs-client-tafj
helm install ndcs-smtp ndcs-smtp
helm install ndcs-db ndcs-db
helm install ndcs-server ndcs-server
```

- Step 4: Modify /etc/drivers/hosts. Add these line:

```
127.0.0.1 tafj.k8s.local
127.0.0.1 tafjdb.k8s.local
127.0.0.1 smtp.k8s.local
127.0.0.1 ndcsserver.k8s.local
```

**Notes**: In order to clean up, run the following commands:

```
helm uninstall ndcs-client-tafc ndcs-client-tafc
helm uninstall ndcsclienttafjdb ndcsclienttafjdb
helm uninstall ndcs-client-tafj ndcs-client-tafj
helm uninstall ndcs-smtp ndcs-smtp
helm uninstall ndcs-db ndcs-db
helm uninstall ndcs-server ndcs-server
```

## Overview results

![server](images/server.PNG)
![tafj](images/tafj.PNG)
![smtp](images/smtp.PNG)

## Future steps

- [ ] Integrate helm flows with ci-cd pipeline
- [ ] Create kubernetes cluster on cloud provider or private server 
- [ ] Better design with specific cloud provider

## Example Azure K8s

![](https://docs.microsoft.com/en-us/azure/architecture/reference-architectures/containers/aks/images/secure-baseline-architecture.svg)

This architecture uses a hub-spoke network topology. The hub and spoke(s) are deployed in separate virtual networks connected through peering. Some advantages of this topology are:

- Segregated management. It allows for a way to apply governance and control the blast radius. It also supports the concept of landing zone with separation of duties.

- Minimizes direct exposure of Azure resources to the public internet.

- Organizations often operate with regional hub-spoke topologies. Hub-spoke network topologies can be expanded in the future and provide workload isolation.

- All web applications should require a web application firewall (WAF) service to help govern HTTP traffic flows.

- A natural choice for workloads that span multiple subscriptions.

- It makes the architecture extensible. To accommodate new features or workloads, new spokes can be added instead of redesigning the network topology.

- Certain resources, such as a firewall and DNS can be shared across networks.

